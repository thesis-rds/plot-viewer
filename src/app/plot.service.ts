import { Injectable, EventEmitter } from '@angular/core';
import { Observable, of, concat } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { environment } from '../environments/environment';

export class Plot {
  data: any[];
  layout: any;
  meta: any;
}

export type SelectPlotEvent = EventEmitter<{type?: string, name: string}>;

var httpOptions = {
  headers: {
    'Content-Type':   'application/json',
    'Accept':         'application/json'
  }
};

@Injectable({
  providedIn: 'root'
})
export class PlotService {
  treeChange: EventEmitter<any> = new EventEmitter();
  isFetching: boolean = false;
  index: any = {};
  tree: any = [];
  constructor(
    private http: HttpClient,
  ) {
    this.isFetching = true;
    this.fetchIndex().subscribe(
      index => {
        this.index = index;
        this.tree = this.processIndex(this.index);
        this.treeChange.emit(this.tree);
        this.isFetching = false;
      }
    )
  }

  private fetchIndex(): Observable<any> {
    return this.http.get<any>(environment.apiHost, httpOptions);
  }

  getPlot(name: string, type: string): Observable<Plot> {
    this.isFetching = true;
    // console.log("Fetching plot n:'"+name+"', t:'"+type+"' @'"+'localhost:4201/' + type + '/json/' + name + '.json'+"'");
    return this.http.get<Plot>(environment.apiHost + type + '/json/' + name, httpOptions).pipe(
      tap(
        () => this.isFetching = false
      )
    );
  }

  processIndex(index) {
    console.log(index);
    let treeRoot = [];
    let i = 0;
    for(var type_id in index.types) {
      let type = index.types[type_id];
      i++;
      let j = 0;
      let treeNode = {
        id: i * 100000 + j++,
        name: type,
        children: []
      }
      let nodes: any = Object.values(index.meta).filter((node: any) => node.hasType.includes(type));
      for(var node_i in nodes) {
        let node_name = nodes[node_i].name;
        treeNode.children.push({
          id: i * 100000 + j++,
          name: node_name
        });
      }
      treeRoot.push(treeNode)
    }
    console.log(treeRoot);
    return treeRoot;
  }

  private _update_meta(node: string, key: string, value: any): Observable<any> {
    let formData: FormData = new FormData();
    formData.append('key', key);
    formData.append('value', value);
    return this.http.post(environment.apiHost + 'meta/' + node, formData);
  }

  updateMeta(node: any, changes: any): Observable<any> {
    return this.http.post(environment.apiHost + 'meta/' + node.name, changes);
  }

  build(node: any, type: string): Observable<any> {
    return this.http.patch(environment.apiHost + type + '/' + node.name, {});
  }
}
