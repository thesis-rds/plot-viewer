import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TracePlotComponent } from './trace-plot.component';

describe('TracePlotComponent', () => {
  let component: TracePlotComponent;
  let fixture: ComponentFixture<TracePlotComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TracePlotComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TracePlotComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
