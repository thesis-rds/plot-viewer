import { Component, OnInit, Input } from '@angular/core';
import { PlotService, Plot } from '../plot.service';

@Component({
  selector: 'app-trace-plot',
  templateUrl: './trace-plot.component.html',
  styleUrls: ['./trace-plot.component.scss']
})
export class TracePlotComponent implements OnInit {
  @Input() plot: Plot;
  @Input() fetching: boolean = false;


  constructor(

  ) { }

  ngOnInit() {

  }

}
