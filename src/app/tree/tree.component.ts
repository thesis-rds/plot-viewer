import { FlatTreeControl } from '@angular/cdk/tree';
import { Component, Input, Output, EventEmitter } from '@angular/core';
import { MatTreeFlatDataSource, MatTreeFlattener } from '@angular/material/tree';
import { SelectPlotEvent } from '../plot.service';

/**
 * @title Tree with flat nodes
 */
@Component({
  selector: 'app-tree',
  templateUrl: 'tree.component.html'
})
export class TreeComponent {
  @Output() plotSelect: SelectPlotEvent = new EventEmitter();
  @Input() nodes: any[] = [];
  options = {};

  constructor() {
  }

  onActivate(name: string) {
    this.plotSelect.emit({ name: name});
  }
}
