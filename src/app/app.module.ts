import { HttpClientModule }    from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule }   from '@angular/forms';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { PlotlyViaCDNModule } from 'angular-plotly.js';

import { TreeModule } from 'angular-tree-component';

import { MatTreeModule } from '@angular/material/tree';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatCheckboxModule} from '@angular/material/checkbox';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TracePlotComponent } from './trace-plot/trace-plot.component';
import { LoaderComponent } from './loader/loader.component';
import { TreeComponent } from './tree/tree.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AngularResizedEventModule } from 'angular-resize-event';
import { ManagePlotComponent } from './manage-plot/manage-plot.component';
import { MiniCaptionPipe } from './mini-caption.pipe';

@NgModule({
  declarations: [
    AppComponent,
    TracePlotComponent,
    LoaderComponent,
    TreeComponent,
    ManagePlotComponent,
    MiniCaptionPipe
  ],
  imports: [
    MatTreeModule,
    MatIconModule,
    MatInputModule,
    MatButtonModule,
    MatProgressSpinnerModule,
    MatAutocompleteModule,
    MatCheckboxModule,

    NgbModule,
    FormsModule,
    TreeModule.forRoot(),
    ReactiveFormsModule,
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    PlotlyViaCDNModule.forRoot({version: 'latest'}),
    BrowserAnimationsModule,
    AngularResizedEventModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
