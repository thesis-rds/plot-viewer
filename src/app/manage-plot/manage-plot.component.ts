import { Component, OnInit, OnChanges, SimpleChanges, Input, Output, EventEmitter } from '@angular/core';
import { PlotService, Plot, SelectPlotEvent } from '../plot.service';

function isNumber(n) {
  return !isNaN(parseFloat(n)) && isFinite(n);
}

@Component({
  selector: 'app-manage-plot',
  templateUrl: './manage-plot.component.html',
  styleUrls: ['./manage-plot.component.scss']
})
export class ManagePlotComponent implements OnInit {
  @Input() node: any;
  @Output() plotSelect: SelectPlotEvent = new EventEmitter();
  @Output() nodeChange: EventEmitter<any> = new EventEmitter();
  objectKeys = Object.keys;
  nodeState: any = {};
  isBuilding: boolean = false;
  isUpdating: boolean = false;
  activeType: string = 'raw';
  expanded: boolean = false;

  metaKeys: string[] = ['final_peak_window', 'curation', 'final_peak_selector', 'name'];

  constructor(
    private plots: PlotService
  ) { }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges) {
    if('node' in changes) {
      this.nodeState = {};
      if(changes.node.currentValue != undefined) {
        Object.keys(changes.node.currentValue).map(x => this.nodeState[x] = changes.node.currentValue[x]);
      }
    }
  }

  selectPlotType(type: string) {
    this.activeType = type;
    this.plotSelect.emit({name: this.node.name, type: type});
  }

  saveMeta() {
    let changes: any = {};
    for(var key in this.nodeState) {
      if(this.nodeState[key] != this.node[key]) {
        changes[key] = isNumber(this.nodeState[key]) ? parseFloat(this.nodeState[key]) : this.nodeState[key];
      }
    }
    if("peaks_selector" in changes) {
      // Turns "1, 2, 3" into [1, 2, 3]
      changes["peaks_selector"] = changes["peaks_selector"].split(",").map(x => parseInt(x));
      if(!isNumber(changes["peaks_selector"][0]))
        changes["peaks_selector"] = [];
    }
    if("final_peak_selector" in changes) {
      changes["final_peak_selector"] = changes["final_peak_selector"].split(",").map(x => parseInt(x));
      if(!isNumber(changes["final_peak_selector"][0]))
        changes["final_peak_selector"] = [];
    }
    this.isUpdating = true;
    this.plots.updateMeta(this.node, changes).subscribe(
      success => {
        this.isUpdating = false;
        for(var key in this.nodeState) {
          this.node[key] = this.nodeState[key];
        }
        this.nodeChange.emit(null);
      },
      err => {
        this.isUpdating = false;
        for(var key in this.nodeState) {
          this.nodeState[key] = this.node[key];
        }
      }
    );
  }

  confirmPlot() {
    this.isUpdating = true;
    this.plots.updateMeta(this.node, {curation: this.activeType}).subscribe(
      success => {
        this.isUpdating = false;
        for(var key in this.nodeState) {
          this.node.curation = this.activeType;
          this.nodeState.curation = this.activeType;
        }
        this.nodeChange.emit(null);
      }
    );
  }

  buildPlot() {
    this.saveMeta();
    this.isBuilding = true;
    this.plots.build(this.node, this.activeType).subscribe(
      response => {
        this.selectPlotType(this.activeType);
        this.isBuilding = false;
      }
    );
  }
}
