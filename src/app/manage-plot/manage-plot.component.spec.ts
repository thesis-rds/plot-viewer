import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManagePlotComponent } from './manage-plot.component';

describe('ManagePlotComponent', () => {
  let component: ManagePlotComponent;
  let fixture: ComponentFixture<ManagePlotComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManagePlotComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManagePlotComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
