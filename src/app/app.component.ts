import { Component, ViewChild, ElementRef, OnInit, EventEmitter } from '@angular/core';
import { Observable, of, timer } from 'rxjs';
import { startWith, map, debounce, tap } from 'rxjs/operators';
import { PlotService, Plot} from './plot.service';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  @ViewChild('plotCol') plotCol: ElementRef;
  innerChanges = new EventEmitter();
  nodeSearch = new FormControl('');
  acNodes: Observable<any>;
  public activePlot: Plot;
  public activeNode: any;
  public activeType: any;
  public plotWidth: number = 0;
  public plotHeight: number = 0;
  treeFiltered: any = [];
  filtersCollapsed: boolean = true;
  steps: string[] = ["raw","filtered","annotated","deconvoluted","final","minimized"];
  grades: string[] = ["bad","doable","good","perfect"];
  filterSteps: any = {};
  filterGrades: any = {};
  oVal = Object.values;

  constructor (
    public fp: PlotService
  ) {
    this.steps.map(x => this.filterSteps[x] = true);
    this.grades.map(x => this.filterGrades[x] = true);
    fp.treeChange.subscribe(
      tree => this.applyTreeFilters()
    )
  }

  toggleFilter(obj, key) {
    obj[key] = !obj[key];
    this.applyTreeFilters();
  }

  applyTreeFilters() {
    this.treeFiltered = Object.values(this.fp.index.meta).filter((node: any) => {
      return this.filterGrades[node.grade] && this.filterSteps[node.curation];
    });
  }

  ngOnInit() {
    this.innerChanges.pipe(
      debounce(() => timer(700))
    ).subscribe(value => {
      if(this.fp.index.meta && typeof(value) == 'object' && Object.values(value).length > 0) {
        this.onPlotSelect({name: Object.values(value)[0].name});
      }
    });
    this.acNodes = this.nodeSearch.valueChanges.pipe(
      startWith(''),
      map(value => this.filterNodes(value)),
      tap(value => {
        this.innerChanges.emit(value);
      })
    );
  }

  getActiveType(): string {
    return this.activeType || 'raw';
  }

  onPlotSelect(event) {
    this.activePlot = undefined;
    this.activeNode = Object.values(this.fp.index.meta).filter((node: any) => node.name == event.name)[0];
    let eType = event.type || this.getActiveType();
    this.activeType = eType;
    this.fp.getPlot(event.name, eType).subscribe(
      plot => {
        plot.layout.width = this.plotWidth;
        plot.layout.height = this.plotHeight;
        this.activePlot = plot;
      }
    )
  }

  onResize(event) {
    this.plotWidth = event.newWidth;
    this.plotHeight = event.newHeight;
    if(this.activePlot) {
      this.activePlot.layout.width = event.newWidth;
      this.activePlot.layout.height = event.newHeight;
    }
  }

  private filterNodes(value: string): string[] {
    const filterValue = value.toLowerCase();
    if(!this.fp.index.meta) return [];
    return <string[]>Object.values(this.fp.index.meta).filter((node: any) => node.name.toLowerCase().includes(filterValue));
  }
}
